#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/tracking.hpp>

using namespace cv;
using namespace dnn;
using namespace std;

float objectnessThreshold = 0.1;
float confThreshold = 0.5;
float nmsThreshold = 0.4;
int inpWidth = 416;
int inpHeight = 416;
vector<string> classes;
Net net;
string MODEL_PATH = "./data/";

void init_detector() {
  String modelConfiguration = MODEL_PATH + "yolov3.cfg";
  String modelWeights = MODEL_PATH + "yolov3.weights";

  net = readNetFromDarknet(modelConfiguration, modelWeights);
}

vector<String> getOutputsNames(const Net &net) {
  static vector<String> names;
  if (names.empty()) {
    vector<int> outLayers = net.getUnconnectedOutLayers();

    vector<String> layersNames = net.getLayerNames();

    names.resize(outLayers.size());
    for (size_t i = 0; i < outLayers.size(); ++i)
      names[i] = layersNames[outLayers[i] - 1];
  }
  return names;
}

Rect detect_ball(Mat frame) {
  Mat blob;
  blobFromImage(frame, blob, 1 / 255.0, Size(inpWidth, inpHeight),
                Scalar(0, 0, 0), true, false);
  net.setInput(blob);
  vector<Mat> outs;
  net.forward(outs, getOutputsNames(net));

  double maxConfidence = 0;
  // Returned when no ball is detected
  Rect returnRect = Rect(1, 1, 1, 1);

  for (size_t i = 0; i < outs.size(); ++i) {
    float *data = (float *)outs[i].data;
    for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols) {
      float objectness = outs[i].at<float>(j, 4);
      if (objectness > objectnessThreshold) {
        // We are only interested in the 'sports ball' scores
        // sports ball is line 33 in the class file. It's index is 33 + 5 - 1 =
        // 37 in the scores matrix
        Mat scores = outs[i].row(j).colRange(37, 38);
        Point classIdPoint;
        double confidence;
        minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
        if (confidence > maxConfidence) {
          int centerX = (int)(data[0] * frame.cols);
          int centerY = (int)(data[1] * frame.rows);
          int width = (int)(data[2] * frame.cols);
          int height = (int)(data[3] * frame.rows);
          int left = centerX - width / 2;
          int top = centerY - height / 2;
          returnRect = Rect(left, top, width, height);
          maxConfidence = confidence;
        }
      }
    }
  }
  return returnRect;
}

int main(int argc, const char **argv) {

  init_detector();

  std::string input_video = "";
  std::string output_video = "ball_tracked.mp4";

  if (argc == 1) {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "------" << std::endl;
    std::cerr << "ball_tracker INPUT_VIDEO [OUTPUT_VIDEO]" << std::endl;
    return 1;
  }

  if (argc > 1) {
    input_video = argv[1];
  }
  if (argc > 2) {
    output_video = argv[2];
  }

  Mat frame;

  VideoCapture video;
  video.open(input_video);
  double width = video.get(CAP_PROP_FRAME_WIDTH);
  double height = video.get(CAP_PROP_FRAME_HEIGHT);
  double fps = video.get(CAP_PROP_FPS);

  cv::VideoWriter out(output_video, cv::VideoWriter::fourcc('X', 'V', 'I', 'D'),
                      fps, cv::Size(width, height));

  Ptr<Tracker> tracker;
  bool tracked = true;

  while (video.isOpened()) {
    int frame_number = video.get(CAP_PROP_POS_FRAMES);
    video >> frame;

    if (frame.empty())
      break;

    // We detect every 10th frame, or when tracking fails
    if (frame_number % 10 == 0 || !tracked) {
      Rect bounding_box = detect_ball(frame);
      tracker = TrackerKCF::create();
      tracker->init(frame, bounding_box);

      // Check if detecting succeeded... if not the rectangle has width and
      // height of 1
      if (bounding_box.width != 1 && bounding_box.height != 1) {
        rectangle(frame, bounding_box, Scalar(0, 255, 0));
        tracked = true;
      }
    } else {
      Rect bounding_box;
      tracked = tracker->update(frame, bounding_box);

      if (tracked) {
        rectangle(frame, bounding_box, Scalar(0, 255, 0), 2, 1);
      }
    }
    if (!tracked) {
      putText(frame, "Tracking failure detected", Point(100, 80),
              FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0, 0, 255), 2);
    }

    string window_name = "Ball Tracking";
    imshow(window_name, frame);
    waitKey(100);

    out.write(frame);
  }
  video.release();
  return 0;
}
