# Ball Tracker

## Example

[![Ball Tracker](http://img.youtube.com/vi/JGqXwAZ8Mcg/0.jpg)](http://www.youtube.com/watch?v=JGqXwAZ8Mcg "Ball Tracker")

## Using the project

- Clone the Git repository
```bash
git clone https://gitlab.com/tjaart/ball_tracker.git
```

- Initialize the project

```bash
cmake .
```

- Compile the project
```bash
cmake --build .
```

- Run the tracker

```bash
./ball_tracker my_ball_video.mp4 my_ball_output.mp4
```
